####################################
## ADD NEW RECORDS TO DATASET
####################################

#THE AIM OF THIS SCRIPT IS TO INTRODUCE NEW RECORDS TO THE EXISTING OUTPUT TABLE
#NEW RECORDS ARE THOSE IN ANY OF THE INPUT TABLES WHOSE ID IS NOT IN THE OUTPUT TABLE

#INSTALL PACKAGES
packages <- c("RecordLinkage", "dplyr", "odbc")
if ( length(missing_pkgs <- setdiff(packages, rownames(installed.packages()))) > 0) {
  message("Installing missing package(s): ", paste(missing_pkgs, collapse = ", "))
  install.packages(missing_pkgs)
}

library(RecordLinkage)
library(dplyr)
library(odbc)

# ESTABLISH CONNECTION
con <- dbConnect(odbc(),
                 Driver = "SQL Server",
                 Server = "mandc-sql-01",
                 Database = "SEE_REP",
                 Trusted_Connection = "True")

# CHOOSE DATASETS FOR MODEL
dataset <- dbGetQuery(con,'select * FROM [SEE_REP].[dbo].[OG_Output] order by email')
NewData <- dbGetQuery(con,'select * FROM [dbo].[OG_NewRecords]')

#CHECK WHETHER THERE ARE ANY NEW RECORDS BEFORE PROCEEDING
if(length(NewData$SourceID) == 0){
  print("No new records to add.")
  
}else{

  #==================================
  ## CLEANSING
  #==================================

  # ENSURE FIELDS ARE IN THE CORRECT R FORMAT
  NewData$telh           <- as.numeric(NewData$telh)
  NewData$mobile         <- as.numeric(NewData$mobile)
  NewData$updatedate     <- as.POSIXct(NewData$updatedate,tz=Sys.timezone())
  NewData$sourcemodified <- as.POSIXct(NewData$sourcemodified,tz=Sys.timezone())
  dataset$PersistedID    <- as.integer(dataset$PersistedID)
  
  
  ## BLANKS
  NewData[1:17][NewData[1:17] == ""] <- NA
  
  
  LoopCount <- 1
  HurdleWeight <- 0.5
  
  # Set IsMatched field to 0 for previously matched errors
  NewData$IsMatched <- 0
  
  #==============================================================================
  # MOVE DATA INTO TEMP TABLES TO BE EXECUTED ON
  #==============================================================================
  TempDataset <- dataset
  TempNewData <- NewData
  
  #==============================================================================
  # SET THE FIELDS YOU WANT TO BLOCK ON
  #==============================================================================
  
  blockfields <- c('email','add1','postcode','add1','postcode','telh','mobile','mm')
  
  
  print(paste("Initial number of new records:", length(NewData$PersistedID)))
  print(paste("Initial number of records in dataset:", length(dataset$PersistedID)))
  
  ############################################################################################################
  # MAIN EXECUTION OF MATCHING LOOP
  # EACH TIME A RECORD IS MATCHED IT IS ADDED TO THE MAIN DATA TABLE AND REMOVED FROM THE NEW DATA TABLE
  
  #CONDITIONS TO BE MET BEFORE THE MATCHING ALGORITHM IS RUN:
  #1.THERE MUST BE NO DUPLICATE IDS IN THE MAIN DATASET
  #2.THERE MUST BE NO DUPLICATE IDS IN THE NEW DATASET 
  #3.THERE MUST BE NO IDS IN THE MAIN DATASET THAT ALSO APPEAR IN THE NEW DATASET, AND VICE VERSA
  
  #Check of for duplicates in TempDataset
  if(length(unique(TempDataset$PersistedID)) == length(TempDataset$PersistedID) & length(unique(TempDataset$SourceID)) == length(TempDataset$SourceID)){
    #Check for duplicates in TempNewData
    if(length(unique(TempNewData$SourceID)) == length(TempNewData$SourceID) & length(unique(TempNewData$PersistedID)) == length(TempNewData$PersistedID)){
      #Check for overlapping PersistedIDs and SourceIDs between two datasets
      if(length(TempDataset$PersistedID[TempDataset$PersistedID %in% TempNewData$PersistedID]) == 0 & length(TempDataset$SourceID[TempDataset$SourceID %in% TempNewData$SourceID]) == 0){
        
        
        for (i in 1:length(blockfields)) {
          
          print("===============================================")
          print(paste("Blocking on :", blockfields[i]))
          print("===============================================")
          
          #EXECUTION OF MATCHING ALGORITHM
          
          rpairs_dedupe <-  try(RLBigDataLinkage(   TempDataset, TempNewData
                                                    , identity1 = TempDataset$email, identity2 = TempNewData$email
                                                    , exclude = c(1,2,3,4,  17,18,19)
                                                    , blockfld = append(blockfields[i],c('finitial','linitial'))
                                                    , strcmp = TRUE
                                                    , strcmpfun = "levenshtein"
                                                    # , phonetic = c(5:6), phonfun = "pho_h"
          ),silent = TRUE)
          
          if (class(rpairs_dedupe) == "try-error"){
            print("No matches found. Skipping to the next blocker.")
            
          }else{
            # TOTAL RECORDS AND PAIRS
            print(rpairs_dedupe)
            
            #================================
            ## EM ALGORITHM
            #================================
            
            ## Add weights of matches
            rpairs_dedupe <- epiWeights(rpairs_dedupe)
            
            ## Classify weights over HurdleWeight as matches
            rpairs_dedupe <- epiClassify(rpairs_dedupe, HurdleWeight)
            
            # GETTING LINKS
            matchedPairs <- getPairs(rpairs_dedupe,min.weight = HurdleWeight, single.rows=T)
            
            #======================================================
            # CHECK WHETHER ANY MATCHES MEET THE MINIMUM CRITERIA
            #======================================================
            if(length(matchedPairs$SourceID.1) == 0){
              print("No matches pass hurdle rate. Skipping to the next blocker.")
            }else{
              
              #======================================================================================
              ## DATA IS IN 3 TABLES, LEFT SIDE OF MATCH, RIGHT SIDE OF MATCH AND UNMATCHED RECORDS,
              ## THESE ARE UNIONED, GOLDEN IDS ARE CREATED AND ISMATCHED FLAG IS SET
              #======================================================================================
              
              dataset1 <- matchedPairs %>% select(id.1, NewID.1, PersistedID.1, IsMatched.1, SourceID.1, fname.1, lname.1, email.1, add1.1, postcode.1, telh.1, mobile.1, finitial.1, linitial.1, 
                                                  yyyy.1, mm.1, dd.1, tablesource.1, updatedate.1, sourcemodified.1)
              
              colnames(dataset1)  <- c("GoldenID", "NewID", "PersistedID", "IsMatched" ,"SourceID", "fname", "lname", "email", "add1", "postcode", "telh", "mobile", "finitial", "linitial", 
                                       "yyyy", "mm", "dd", "tablesource", "updatedate", "sourcemodified")
              
              dataset1$IsMatched <- 1
              
              print(paste("Dataset1 Length:", length(dataset1$PersistedID)))
              
              
              dataset2 <- matchedPairs %>% select(id.1, NewID.2, PersistedID.2, IsMatched.2, SourceID.2, fname.2, lname.2, email.2, add1.2, postcode.2, telh.2, mobile.2, finitial.2, linitial.2, 
                                                  yyyy.2, mm.2, dd.2, tablesource.2, updatedate.2, sourcemodified.2)
              
              colnames(dataset2)  <- c("GoldenID", "NewID", "PersistedID", "IsMatched", "SourceID", "fname", "lname", "email", "add1", "postcode", "telh", "mobile", "finitial", "linitial", 
                                       "yyyy", "mm", "dd", "tablesource", "updatedate", "sourcemodified")
              
              dataset2$IsMatched <- 1
              
              print(paste("Dataset2 Length:", length(dataset2$PersistedID)))
              
              # Get all records from original dataset that do not have appear in either of the matched lists
              Unmatched <- subset(TempDataset, !(TempDataset$SourceID %in% dataset1$SourceID | TempDataset$SourceID %in% dataset2$SourceID))
              
              
              # Create dummy TempGoldenID to append unmatched data back on to matches
              GoldenID <- as.integer(NA)
              
              Unmatched <- cbind(GoldenID, Unmatched)
              
              # Re-assemble 3 parts
              output <- rbind(dataset1, dataset2, Unmatched)
              
              # Set TempGoldenID field as an Integer
              output$GoldenID <- as.integer(output$GoldenID)
              output$NewID <- as.integer(output$NewID)
              
              
              #===================================================================
              # Grouping by GoldenID - Set NewID to MIN(NewID) grouped by GoldenID
              #===================================================================
              
              # Set all NewIDs to the lowest one, grouped by the GoldenID
              output$NewID <- as.integer(with(output, ave(NewID, GoldenID, FUN=function(f) min(f, na.rm=T))))
              
              # Set all NewIDs to the lowest one, grouped by the SourceID
              output$NewID <- as.integer(with(output, ave(NewID, SourceID, FUN=function(f) min(f, na.rm=T))))
              
              #===================================================================
              
              
              # Deduplicate on SourceID
              output <- output[!duplicated(output$SourceID),]
              
              #Drop TempGoldenID column
              output <- select(output, -c(GoldenID))                            #<-   REMOVE MinID TOO IF YOU RUN METHOD 2
              
              print(paste("Output Length:", length(output$PersistedID)))
              
              NewIDCounts <- count(output[output$IsMatched == 1,], NewID)
              print(paste("IDs left behind:",length(NewIDCounts$NewID[NewIDCounts$n == 1])))
              
              #Errors <- output[output$NewID %in% NewIDCounts$NewID[NewIDCounts$n == 2],]
              
              
              #=====================================
              # OUTPUTS & MESSAGES
              #=====================================
              TempDataset <- output
              
              length(NewData$PersistedID)
              #TempNewData[ TempNewData$PersistedID %in% TempDataset$PersistedID, ]
              
              TempNewData <- subset(TempNewData, !(TempNewData$PersistedID %in% TempDataset$PersistedID))
              
              print(paste("IDs remaining in New Data Table:", length(TempNewData$PersistedID)))
              
              print(paste("!!! Total records:", length(TempDataset$PersistedID) + length(TempNewData$PersistedID),"!!!"))
              print(paste("!!! Original records:", length(dataset$PersistedID) + length(NewData$PersistedID),"!!!"))
              
              length(NewData$PersistedID) + length(dataset$PersistedID)
              
              dataset1      <- NULL
              dataset2      <- NULL
              Unmatched     <- NULL
              matchedPairs  <- NULL
              rpairs_dedupe <- NULL
              output        <- NULL
              SampleDataset <- NULL
              UniqueMins    <- NULL
              #NewIDCounts   <- NULL
              #Errors        <- NULL
            }
          }
          LoopCount <- LoopCount +1
        }
        
      }else{
        print("Tempdataset and Errordataset have overlapping records! Aborting output!")
      }
    }else{
      print("NewDataset has duplicates! Aborting output!")
    }
  }else{
    print("Tempdataset has duplicates! Aborting output!")
  }
  
  
  #===================================================================================================================
  #FINAL CHECKS
  
  #BEFORE OUTPUTTING CERTAIN CRITERIA MST BE MET:
  #1. THERE MUST STILL BE NO DUPLICATES IN THE MAIN DATASET
  #2. THERE MUST STILL BE NO DUPLICATES IN THE NEW DATASET
  #3. THERE MUST BE NO OVERLAPPING IDS BETWEEN THE TWO DATASETS
  #4. THE NUMBER OF RECORDS ENTERING THE ALGORITHM MUST EQUAL THE TOTAL NUMBER OF RECORDS BEING OUTPUT TO OG_OUTPUT AND OG_ERRORS 
  
  #Check for duplicates in TempDataset
  if(length(unique(TempDataset$PersistedID)) == length(TempDataset$PersistedID) & length(unique(TempDataset$SourceID)) == length(TempDataset$SourceID)){
    #Check for duplicates in TempNewData
    if(length(unique(TempNewData$SourceID)) == length(TempNewData$SourceID) & length(unique(TempNewData$PersistedID)) == length(TempNewData$PersistedID)){
      #Check for overlapping PersistedIDs and SourceIDs between two datasets
      if(length(TempDataset$PersistedID[TempDataset$PersistedID %in% TempNewData$PersistedID]) == 0 & length(TempDataset$SourceID[TempDataset$SourceID %in% TempNewData$SourceID]) == 0){
        #If the number of records going in is the same as the number of records coming out
        if(length(TempNewData$PersistedID) + length(TempDataset$PersistedID) == length(NewData$PersistedID) + length(dataset$PersistedID)){
          
          #Union the main dataset and the new data
          TempDataset <- rbind(TempDataset, TempNewData)
          
          #Send stray matched records in the main table to OG_Errors SQL table
          NewIDCounts <- count(TempDataset[TempDataset$IsMatched == 1,], NewID) #create counts of matched NewIDs
          Errors <- TempDataset[TempDataset$NewID %in% NewIDCounts$NewID[NewIDCounts$n == 1],]
          
          print(paste("Outputting",length(Errors$PersistedID), "records to OG_Errors. "))
          dbWriteTable(con, "OG_Errors", Errors, overwrite = TRUE, row.names = FALSE)
          
          #Output main table to SQL
          print(paste("Outputting",length(TempDataset$PersistedID), "records to OG_Output. "))
          dbWriteTable(con, "OG_Output", TempDataset, overwrite = TRUE, row.names = FALSE)
          
          
        }else{
          print("There are still stray matched records! Cancelling output!")
        }
      }else{
        print("Tempdataset and Errordataset have overlapping records! Aborting output!")
      }
    }else{
      print("NewDataset has duplicates! Aborting output!")
    }
  }else{
    print("Tempdataset has duplicates! Aborting output!")
  }
}

