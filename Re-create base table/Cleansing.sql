---- ======================================================================= --
----								DATA CLEANSING						
---- ======================================================================= --



-- Deduplicate the retail customer table
-- drop table #RetailCustomer
select *, ROW_NUMBER() over(partition by CustomerID order by ChangeDate desc, DateImported desc) rn
into #RetailCustomer
from [CityFootball_Master_Staging].[Kitbag].[Customer]
delete from #RetailCustomer where rn > 1


-- drop table #GoldenIDBase
select * 
into #GoldenIDBase
from
(
-- select top 100 * from sro.Clients
--select top 100000
--NULL                                   as GoldenID,
--'SRO_' + cast(SRO_clCrmId as varchar)  as SourceID,
--FirstName                              as fname,
--LastName                               as lname,
--email                                  as email,
--AddressLine1                           as add1,
--PostalCode                             as postcode,
--HomePhone                              as telh,
--MobilePhone                            as mobile,
--left(FirstName,3)                      as finitial,
--left(LastName,3)                       as linitial,
--YEAR(Birthday)                         as yyyy,
--MONTH(Birthday)                        as mm,
--DAY(Birthday)                          as dd,
--'SRO'                                  as tablesource,
--getdate()                              as updatedate,
--LastUpdated_DateTime                   as sourcemodified
--from sro.Clients

--UNION ALL

--select top 100 * from Customer

select 
--NULL                               as GoldenID,
'SEE_' + cast(memno as varchar)    as SourceID,
fname                              as fname,
lname                              as lname,
email                              as email,
add1                               as add1,
addpost                            as postcode,
telh                               as telh,
mobile                             as mobile,
left(fname,3)                      as finitial,
left(lname,3)                      as linitial,
YEAR(convert(datetime, dob, 103))  as yyyy,
MONTH(convert(datetime, dob, 103)) as mm,
DAY(convert(datetime, dob, 103))   as dd,
'SEE'                              as tablesource,
getdate()                          as updatedate,
convert(datetime, modifydate, 103) as sourcemodified
from Customer
where memno in (select distinct memno from ticktranstable 
                where convert(date, daten, 103) between '2015-07-01' and '2018-07-31'
		       )


UNION ALL

select
--NULL                                 as GoldenID,
'RET_' + cast(CustomerID as varchar) as SourceID,
FirstName                            as fname,
Surname                              as lname,
email                                as email,
AddressLine1                         as add1,
PostZipCode                          as postcode,
Telephone                            as telh,
TelephoneMobile                      as mobile,
left(FirstName,3)                    as finitial,
left(Surname,3)                      as linitial,
YEAR(DateOfBirth)                    as yyyy,
MONTH(DateOfBirth)                   as mm,
DAY(DateOfBirth)                     as dd,
'Retail'                             as tablesource,
getdate()                            as updatedate,
DateImported                         as sourcemodified
from #RetailCustomer
where CustomerID in 
                   (
                   select distinct CustomerID 
                   from [CityFootball_Master_Staging].[Kitbag].[OrderHeader]
                   where OrderDate between '2015-07-01' and '2018-07-31' and DespatchedDate is not null
                   )
)x

	--=======================================================================================================================
	-- 01.01 - SUPPORTERS DATA Input
	--=======================================================================================================================
update #GoldenIDBase
set 
SourceID	   = ltrim(rtrim(lower(SourceID	                             ))),		 
fname		   = ltrim(rtrim(lower(fname                                 ))),		 
lname		   = ltrim(rtrim(lower(lname                                 ))),		 
email		   = ltrim(rtrim(lower(email                                 ))),		 
add1		   = ltrim(rtrim(lower(add1                                  ))),		 
postcode	   = ltrim(rtrim(lower(replace(postcode, ' ','')             ))),	 
telh		   = ltrim(rtrim(lower([dbo].[fn_ANnormalisePhoneNo](telh)   ))),		 
mobile		   = ltrim(rtrim(lower([dbo].[fn_ANnormalisePhoneNo](mobile) ))),	  
finitial	   = ltrim(rtrim(lower(finitial                              ))),	 
linitial       = ltrim(rtrim(lower(linitial                              ))), 
yyyy		   = ltrim(rtrim(lower(yyyy                                  ))),	 
mm			   = ltrim(rtrim(lower(mm	                                 ))),	  
dd			   = ltrim(rtrim(lower(dd                                    ))),	  
tablesource    = ltrim(rtrim(lower(tablesource                           ))),
updatedate	   = ltrim(rtrim(lower(updatedate                            ))),  
sourcemodified = ltrim(rtrim(lower(sourcemodified                        ))) 

	
CREATE CLUSTERED INDEX IX_TEMP ON #GoldenIDBase (SourceID, tablesource);		

--=======================================================================================================================
-- 01.04 - Email Cleansing
--=======================================================================================================================
	--<< Emails
		--No emails
UPDATE #GoldenIDBase 
SET email = NULL 
WHERE  email LIKE '%@address.co.uk'
    OR email LIKE '%noemail%'
    OR email LIKE '%nomail%'
    OR email LIKE '%noname%'
    OR email LIKE '%email.com'
    OR email LIKE 'none%'
    OR email LIKE 'confirmednoemail%'
    OR email LIKE 'confirmnoemail%'
    OR email LIKE 'confirmnoe-mail%'
    OR email LIKE 'no@%'
    OR email LIKE '%@mcfc.co%'
    OR email LIKE '%@non.co%'
    OR email LIKE '%@none.co%'
    OR email LIKE '%@mancity.co%'
    OR email LIKE '%@na.co%'
    OR email LIKE '%noemail%'
    OR email LIKE '%@test.co%'
    OR email LIKE '%1@1.co%'
    OR email LIKE '%blankemail%'
    OR email LIKE 'null@%'
    OR email LIKE 'blankemail@%'
    OR email = '@membership'
    OR email = '@'
    OR email = '@@'
    OR email = '/@'
    OR email = 'p@'
    OR email = 'j@'
    OR email = 'n/a'
	OR email = 'NA'
    OR email = ''
					 

		
--Remove Internals
DELETE FROM #GoldenIDBase WHERE email IN ('tom.mahon@mancity.com','tom.mahon@mcfc.co.uk','steve.robinson@mancity.com','dupe@mcfc.co.uk')

--Remove No replies
DELETE FROM #GoldenIDBase WHERE email LIKE '%noreply%'

--Remove Tests
DELETE FROM #GoldenIDBase WHERE email LIKE 'test@%'
							 OR email LIKE 'tester@%'
							 OR email LIKE '%@test.com'
							 OR email LIKE '%seatgeek.com'
							 OR fname = 'test'
							 OR fname = 'firstname'
							 OR lname = 'test'
							 OR fname = 'lastname'


--=======================================================================================================================
-- 01.04 - Phone Cleansing
--=======================================================================================================================

UPDATE #GoldenIDBase 
SET telh = NULL 
WHERE telh LIKE '0000%'
   OR telh LIKE '%000000%'
   OR telh LIKE '%111111%'
   OR telh LIKE '%222222%'
   OR telh LIKE '%333333%'
   OR telh LIKE '%444444%'
   OR telh LIKE '%555555%'
   OR telh LIKE '%666666%'
   OR telh LIKE '%777777%'
   OR telh LIKE '%888888%'
   OR telh LIKE '%999999%'
   OR telh LIKE ''
   OR len(telh) < 9

UPDATE #GoldenIDBase 
SET mobile = NULL 
WHERE mobile LIKE '0000%'
   OR mobile LIKE '%000000%'
   OR mobile LIKE '%111111%'
   OR mobile LIKE '%222222%'
   OR mobile LIKE '%333333%'
   OR mobile LIKE '%444444%'
   OR mobile LIKE '%555555%'
   OR mobile LIKE '%666666%'
   OR mobile LIKE '%777777%'
   OR mobile LIKE '%888888%'
   OR mobile LIKE '%999999%'
   OR mobile LIKE ''
   OR len(mobile) < 9


UPDATE #GoldenIDBase SET mobile = CASE WHEN LEFT(mobile, 2) = '44' THEN '0' + SUBSTRING(mobile, 3, LEN(mobile)) ELSE mobile END 
UPDATE #GoldenIDBase SET telh = CASE WHEN LEFT(telh, 2) = '44' THEN '0' + SUBSTRING(telh, 3, LEN(telh)) ELSE telh END 

--=======================================================================================================================
-- 01.04 - Other Field Cleansing
--=======================================================================================================================

--select POSTCODE, count(*) 
--from #GoldenIDBase
--group by POSTCODE
--order by 2 desc

UPDATE #GoldenIDBase SET fname =    NULL WHERE fname in    ('','na','n/a','n\a','none','null','blank','nofirstname') 
UPDATE #GoldenIDBase SET lname =    NULL WHERE lname in    ('','na','n/a','n\a','none','null','blank','nolastname') 
UPDATE #GoldenIDBase SET postcode = NULL WHERE postcode in ('','na','n/a','n\a','none','null','blank')  OR postcode LIKE '0000%' OR len(postcode) < 5
UPDATE #GoldenIDBase SET add1 =     NULL WHERE add1 in     ('','na','n/a','n\a','none','null','blank')

--Invalid DOB
UPDATE #GoldenIDBase
SET   yyyy = NULL , mm = NULL, dd = NULL
WHERE yyyy < 1910 
OR    yyyy > 2015
OR   (yyyy = 1975 and mm = 01 and dd = 01)
OR   (yyyy = 1970 and mm = 01 and dd = 01)	


--=======================================================================================================================
-- 01.05 - Other Field Cleansing
--=======================================================================================================================

update #GoldenIDBase SET fname     = [dbo].[OG_RemoveNonAlphaCharacters] (fname)
update #GoldenIDBase SET lname     = [dbo].[OG_RemoveNonAlphaCharacters] (lname)
update #GoldenIDBase SET email     = [dbo].[OG_NormaliseEmail] (email) 
update #GoldenIDBase SET add1      = [dbo].[OG_RemoveNonAlphaCharacters] (add1)
update #GoldenIDBase SET postcode  = [dbo].[OG_RemoveNonAlphaNumericCharacters] (postcode)
update #GoldenIDBase SET telh      = [dbo].[fn_AN_StripNonNumeric] (telh)
update #GoldenIDBase SET mobile    = [dbo].[fn_AN_StripNonNumeric] (mobile)
update #GoldenIDBase SET finitial  = [dbo].[OG_RemoveNonAlphaCharacters] (finitial)
update #GoldenIDBase SET linitial  = [dbo].[OG_RemoveNonAlphaCharacters] (linitial)	

--=======================================================================================================================
-- 01.06 - Delete records with no contact details
--=======================================================================================================================

DELETE FROM #GoldenIDBase
WHERE	  
fname	 is null and	  
lname	 is null and	  
email	 is null and	  
add1	 is null and	  
postcode is null and	  
telh	 is null and	  
mobile	 is null and	  
finitial is null and	  
linitial is null and      
yyyy	 is null and	  
mm		 is null and	  
dd		 is null 	  


-- drop table [dbo].[OG_input]
select row_number() over (order by sourcemodified, sourceID) NewID,
row_number() over (order by sourcemodified, sourceID) PersistedID,
0 as IsMatched,
* 
into dbo.OG_input
from #GoldenIDBase
